import React from "react";
import store from "./redux/store";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "tachyons";

import { SIGN_IN_PATH, MAIN_PATH } from "./constants/paths";
import SignIn from "./containers/auth";
import Navbar from "./components";
import "./App.scss";

function App() {
  const routes = [
    { path: MAIN_PATH, component: SignIn },
    { path: SIGN_IN_PATH, component: SignIn }
  ];
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          <Navbar />
          {routes.map((elem, i) => (
            <Route exact key={i} path={elem.path} component={elem.component} />
          ))}
        </div>
      </Router>
    </Provider>
  );
}

export default App;

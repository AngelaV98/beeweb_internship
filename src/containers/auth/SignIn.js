import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { signInUser } from "../../redux/actions/authActions";

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange = function(e) {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = e => {
    e.preventDefault();
    const { email, password } = this.state;
    const data = { email, password };
    this.props.signInUser(data);
    this.setState({ email: "", password: "" });
  };
  render() {
    return (
      <main className="pa4 flex justify-center items-center vh-100" id="signin">
        <form className="measure center bg-white  shadow-5 br3 w-60">
          <div className="pa0 black signin_title">
            <p>
              <span>
                <i className="fa fa-lock f4 white" />
              </span>
            </p>
            <legend className="f3 fw6 tc w-100 ph0 mh0">Sign In</legend>
          </div>
          <div className="pa4 pt2">
            <fieldset id="sign_in" className="b--transparent ph0 mh0">
              <div className="mt1">
                <input
                  className="pa3 br2 input-reset bg-transparent w-100"
                  type="email"
                  name="email"
                  id="email-address"
                  placeholder="Email Address *"
                  onChange={this.handleChange}
                  value={this.state.email}
                />
              </div>
              <div className="mv3">
                <input
                  className="pa3 br2 input-reset bg-transparent w-100"
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password *"
                  onChange={this.handleChange}
                  value={this.state.password}
                />
              </div>
              <label className="pa0 ma0 lh-copy f6 pointer">
                <input type="checkbox" /> Remember me
              </label>
            </fieldset>
            <div className="">
              <input
                className="ph3 pointer ba br1 white pv2 input-reset w-100 bg-transparent grow pointer f6 dib"
                type="submit"
                value="Sign in"
                onClick={this.handleSubmit}
              />
            </div>
            <div className="lh-copy mt3">
              <Link to="#0" className="f6 link dim black db">
                Sign up
              </Link>
              <Link to="#0" className="f6 link dim black db">
                Forgot your password?
              </Link>
            </div>
          </div>
        </form>
      </main>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth.loading,
  user: state.auth.user,
  errors: state.auth.errors,
  isSignIn: state.auth.isSignIn
});
const mapDispatchToProps = dispatch => {
  return {
    signInUser: data => dispatch(signInUser(data))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);

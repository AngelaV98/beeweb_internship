import { put, takeEvery, call } from "redux-saga/effects";
import axios from "axios";
import { SIGNIN_USER, signInUserSuccess, signInUserFailure } from "../actions/";

//worker
function* signInUserAsync({ payload }) {
  try {
    const data = yield call(() => {
      return axios.get("https://api.github.com/users/octocat");
    });
    yield put(signInUserSuccess(data));
  } catch (err) {
    yield put(signInUserFailure(err));
  }
}
//watcher
export function* signInUserWatcher() {
  yield takeEvery(SIGNIN_USER, signInUserAsync);
}

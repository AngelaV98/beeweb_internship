import {
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNIN_USER_FAILURE
} from "../actions/";

const initialState = {
  isSignIn: false,
  user: null,
  loading: false,
  errors: {}
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SIGNIN_USER: {
      return {
        ...state,
        loading: true
      };
    }
    case SIGNIN_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        user: action.payload,
        isSignIn: true
      };
    }
    case SIGNIN_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        errors: action.error
      };
    }
    default:
      return state;
  }
};

export default authReducer;

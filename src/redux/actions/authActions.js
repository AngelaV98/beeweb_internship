export const SIGNIN_USER = "SIGNIN_USER";
export const SIGNIN_USER_SUCCESS = "SIGNIN_USER_SUCCESS";
export const SIGNIN_USER_FAILURE = "SIGNIN_USER_FAILURE";

export const signInUser = () => ({
  type: SIGNIN_USER
});

export const signInUserSuccess = payload => {
  return {
    type: SIGNIN_USER_SUCCESS,
    payload
  };
};
export const signInUserFailure = error => ({
  type: SIGNIN_USER_FAILURE,
  error
});

import React from "react";
import { Link } from "react-router-dom";

import { MAIN_PATH, SIGN_IN_PATH } from "../constants/paths";

function Navbar() {
  const links = [
    { name: "Home", path: MAIN_PATH },
    { name: "Sign In", path: SIGN_IN_PATH }
  ];

  return (
    <nav className="flex justify-end pa3">
      {links.map((elem, i) => (
        <Link to={elem.path} key={i} className="mr3 f4 dim link">
          {elem.name}
        </Link>
      ))}
    </nav>
  );
}

export default Navbar;
